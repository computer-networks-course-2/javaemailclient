package GUI;

public enum ScreenState {

    LOGIN("Login.fxml"),
    MAILS("EmailsInbox.fxml"),
    SEND_MAILS("SendMail.fxml"),
    CONFIGURATION("Configuration.fxml");

    private String file;

    ScreenState(String file) {
        this.file = file;
    }

    public String getFile() {
        return "src//GUI//" + file;
    }
}
