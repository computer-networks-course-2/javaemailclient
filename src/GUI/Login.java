package GUI;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import javax.mail.*;
import java.util.Properties;

public class Login {

    public TextField email;
    public PasswordField password;
    public Button login;
    public Label loginMessage;
    public Label smtpTarget;
    public Label imapTarget;
    public Button configurationBtn;

    private void login() {
        if (!email.getText().isEmpty() && !password.getText().isEmpty()) {

            String SmtpServerTarget = MailApplication.SMTPServer;

            // Initialize connection to target SMTP server
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", SmtpServerTarget);
            props.put("mail.smtp.port", "587");

            // Check login credentials
            MailApplication.setSmtpsession(Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(email.getText(), password.getText());
                        }
                    }));

            try {
                // Initialize connection to IMAP server
                String imapServerTarget = MailApplication.IMAPServer;

                Session session = Session.getDefaultInstance(new Properties());
                Store store = session.getStore("imaps");
                store.connect(imapServerTarget, 993, email.getText(), password.getText());

                MailApplication.setImapSession(store);

                // Set email to state of application
                MailApplication.setEmail(email.getText());
                MailApplication.setState(ScreenState.MAILS);

            } catch (AuthenticationFailedException afe) {
                this.loginMessage.setText("Login credential incorrect");

            } catch (MessagingException npe) {
                this.loginMessage.setText("Authentication failed");
                npe.printStackTrace();
            }

        } else {
            this.loginMessage.setText("Fill in all login credentials");
        }
    }

    @FXML
    public void initialize() {
        smtpTarget.setText("Current SMTP target: " + MailApplication.SMTPServer);
        imapTarget.setText("Current IMAP target: " + MailApplication.IMAPServer);

        login.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> login());

        configurationBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event ->
                MailApplication.setState(ScreenState.CONFIGURATION));
    }
}
