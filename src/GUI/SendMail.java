package GUI;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;

public class SendMail {
    public Label inputMessage;
    public TextField to;
    public TextField subject;
    public TextArea info;
    public Button send;
    public Button back;
    public Button addAttachment;
    public Label attachmentMessage;
    public FileChooser fileChooser;
    public File selectedAttachment;

    private void sendMail() {
        if (!to.getText().isEmpty() ||
                !subject.getText().isEmpty() ||
                !info.getText().isEmpty()) {


            try {
                // Create a default MimeMessage object.
                Message message = new MimeMessage(MailApplication.getSmtpsession());

                // Set From: header field of the header.
                message.setFrom(new InternetAddress(MailApplication.getEmail()));

                // Set To: header field of the header.
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(to.getText()));

                // Set Subject: header field
                message.setSubject(subject.getText());

                // Create the message part
                BodyPart messageBodyPart = new MimeBodyPart();

                // Now set the actual message
                messageBodyPart.setText(info.getText());

                // Create a multipart message
                Multipart multipart = new MimeMultipart();

                // Set text message part
                multipart.addBodyPart(messageBodyPart);

                // Create new MimeBodyPart for adding attachment to email
                messageBodyPart = new MimeBodyPart();

                // Get file from the selectedAttachment file path
                DataSource source = new FileDataSource(selectedAttachment.getAbsolutePath());
                messageBodyPart.setDataHandler(new DataHandler(source));

                // Give name to the attachment
                messageBodyPart.setFileName(selectedAttachment.getName());

                // Add the attachment to the message
                multipart.addBodyPart(messageBodyPart);

                // Send the complete message parts
                message.setContent(multipart);

                // Send message
                Transport.send(message);

                MailApplication.setState(ScreenState.MAILS);
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }

        } else {
            inputMessage.setText("*Not all fields are filled in");
        }
    }

    private void addSelectedAttachment(File selectedFile) {
        // If selected file is valid, then add file to selectedAttachment
        if (selectedFile != null) {
            this.selectedAttachment = selectedFile;
            this.attachmentMessage.setText("Attachment successfully added");
        }
    }

    @FXML
    public void initialize() {
        fileChooser = new FileChooser();

        send.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> sendMail());

        back.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> MailApplication.setState(ScreenState.MAILS));

        addAttachment.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            File selectedAttachment = fileChooser.showOpenDialog(null);
            addSelectedAttachment(selectedAttachment);
        });
    }
}
