package GUI;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EmailsInbox {

    public Button newEmail;
    public Label inboxMessage;
    public Button logout;
    public ListView<String> mails;

    private void loadInbox() {
        try {
            // Get Inbox of the logged in mail user
            Folder inbox = MailApplication.getImapSession().getFolder("INBOX");
            inbox.open(Folder.READ_ONLY);

            int totalMessagesCount = inbox.getMessageCount();

            // Get the first 10 new messages (sorted on newest)
            Message[] messages = inbox.getMessages(totalMessagesCount - 9, totalMessagesCount);

            // Sort messages from recent to oldest
            Arrays.sort(messages, (m1, m2) -> {
                try {
                    return m2.getSentDate().compareTo(m1.getSentDate());
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            });

            List<String> m = new ArrayList<>();

            // Add message line for List m
            for (Message message : messages) {
                m.add(String.format("DATE: %s, FROM: %s, SUBJECT: %s", message.getSentDate().toString(), Arrays.toString(message.getFrom()), message.getSubject()));
            }

            // Set message lines to this.mails
            this.mails.setItems(FXCollections.observableList(m));

        } catch (MessagingException me) {
            inboxMessage.setText("Could not load emails!");
            me.printStackTrace();
        }
    }

    @FXML
    public void initialize() throws MessagingException {
        loadInbox();

        this.logout.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> MailApplication.setState(ScreenState.LOGIN));

        this.newEmail.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> MailApplication.setState(ScreenState.SEND_MAILS));
    }
}
