package GUI;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Configuration {

    // SMTPServer default
    public static final String DEFAULT_SMTP_SERVER = "smtp.gmail.com";

    // IMAP default
    public static final String DEFAULT_IMAP_SERVER = "imap.googlemail.com";

    private static final String ERROR_CONFIGURATION = "Configuration is not valid!";
    private static final String ERROR_INPUT = "Configuration is not valid!";

    public Button defaultBtn;
    public TextField imapInput;
    public TextField smtpInput;
    public Button cancelBtn;
    public Label errorMessage;
    public Button confirmBtn;

    // Regex
    private Pattern pattern;

    public Configuration() {
        // Regex for checking url
        pattern = Pattern.compile("^(.*\\.com)$");
    }

    // Check if input is valid
    public boolean inputIsValid() {
        // Get inputs
        String imap = imapInput.getText();
        String smtp = imapInput.getText();

        // Check if inputs aren't empty
        if (!imap.isEmpty() && !smtp.isEmpty()) {
            Matcher mImap = pattern.matcher(imap);
            Matcher mSmtp = pattern.matcher(smtp);

            // Check if inputs match the regex
            if (mImap.matches() && mSmtp.matches()) {
                return true;
            }

            errorMessage.setText(ERROR_CONFIGURATION);
            return false;
        }

        errorMessage.setText(ERROR_INPUT);
        return false;
    }

    // Sets config to input
    private void applyConfig() {
        MailApplication.IMAPServer = imapInput.getText();
        MailApplication.SMTPServer = smtpInput.getText();
    }

    // SMTP and IMAP back to original server targets
    private void setConfigToDefault() {
        MailApplication.SMTPServer = DEFAULT_SMTP_SERVER;
        MailApplication.IMAPServer = DEFAULT_IMAP_SERVER;
    }

    @FXML
    public void initialize() {
        defaultBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            setConfigToDefault();
            MailApplication.setState(ScreenState.LOGIN);
        });

        cancelBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event ->
                MailApplication.setState(ScreenState.LOGIN));

        confirmBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (inputIsValid()) {
                applyConfig();
                MailApplication.setState(ScreenState.LOGIN);
            }
        });
    }
}
