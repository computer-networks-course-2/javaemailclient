package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.mail.Session;
import javax.mail.Store;
import java.io.FileInputStream;
import java.io.IOException;

public class MailApplication extends Application {

    private static ScreenState state;

    private static Stage window;

    private static Session smtpsession;
    private static Store imapSession;
    private static String email;

    // SMTPServer target
    public static String SMTPServer = Configuration.DEFAULT_SMTP_SERVER;

    // IMAP target
    public static String IMAPServer = Configuration.DEFAULT_IMAP_SERVER;

    public static void main(String[] args) {
        launch(args);
    }

    public static Store getImapSession() {
        return imapSession;
    }

    public static void setImapSession(Store imapSession) {
        MailApplication.imapSession = imapSession;
    }

    @Override
    public void start(Stage s) {
        window = s;

        window.setResizable(false);
        window.requestFocus();

        window.setTitle("Email Client");

        setState(ScreenState.LOGIN);
    }

    static void setState(ScreenState s) {
        if(s != null && s != state) {
            state = s;

            try {
                FXMLLoader loader = new FXMLLoader();

                Scene scene = new Scene(loader.load(new FileInputStream(s.getFile())));

                window.setScene(scene);

                window.show();
            } catch(IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    static void setSmtpsession(Session s) {
        smtpsession = s;
    }

    public static Session getSmtpsession() {
        return smtpsession;
    }

    public static void setEmail(String email) {
        MailApplication.email = email;
    }

    public static String getEmail() {
        return email;
    }
}
