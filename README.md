# Java Email Client SMTP IMAP

This application is a simple Java Email Client that can check inbox & send mails including attachments.
For Computer Networks 2 course of Avans Informatica P2.4.

## Setup

The min. required JDK and JavaFX is 14.0 in order to run the application.

If you haven't installed Java FX yet, Go to this website and follow the
'JavaFX and Intellij' tutorial:

https://openjfx.io/openjfx-docs/#install-java

Don't forget to initialize the pom.xml file to Maven.
